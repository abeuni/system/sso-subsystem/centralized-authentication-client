import { Module } from 'vuex'
import LoginQuery from '@/gql/LoginQuery.gql'
import UserQuery from '@/gql/UserQuery.gql'
import { apolloClient } from '@/vue-apollo'
import * as jwt from 'jsonwebtoken'
import * as Cookie from 'js-cookie'

export interface User {
  id: string,
  email: string,
  password?: string,
  roles: string[]
}

interface AuthState {
  token: string | null,
  user: User | null
}

export const auth: Module<AuthState, any> = {
  namespaced: true,
  state: {
    token: null,
    user: null
  },
  getters: {
    token (state: AuthState) {
      return state.token
    },
    tokenData (state: AuthState, getters) {
      return jwt.decode(getters.token)
    },
    user (state: AuthState, getters) {
      return state.user
    }
  },
  mutations: {
    loadToken (state: AuthState) {
      const cookie: string | undefined = Cookie.get('authorization')

      if (cookie) { state.token = cookie } else { state.token = null }
    },
    dropToken (state: AuthState) {
      state.token = null
      Cookie.remove('authorization')
    },
    setUser (state: AuthState, user: User | null) {
      state.user = user
    }
  },
  actions: {
    // Module initialization logic
    async init ({ getters, dispatch }: any) {
      console.log('Initializing Auth...')

      // If we have a token but not an user, load user information
      if (getters.token && !getters.user) {
        console.log('Fetching User...')
        await dispatch('loadUser')
      }
    },
    // Logged in user information loading
    async loadUser ({ commit, getters }: any) {
      const token = getters.token

      if (token) {
        const payload = jwt.decode(token)

        if (payload) {
          let user: User | null = null

          try {
            user = (await apolloClient.query({
              query: UserQuery,
              variables: {
                id: payload.sub
              }
            })).data.user
          } catch (e) {
            user = null
          }

          commit('setUser', user)

          return
        }
      }

      commit('setUser', null)
      commit('dropToken')
    },
    async login ({ getters, dispatch, commit }: any, variables: { email: string, password: string }) {
      const result = (await apolloClient.query({
        query: LoginQuery,
        variables
      }))

      if (result && result.data.login && !getters.token) {
        console.log(result)
        Cookie.set('authorization', result.data.login, {
          path: '/',
          secure: process.env.NODE_ENV !== 'development'
        })
      }

      commit('loadToken')
      await dispatch('loadUser')
    },
    async logout ({ commit }: any) {
      commit('setUser', null)
      commit('dropToken')
    }
  }
}
