import Vue from 'vue'
import Router from 'vue-router'
import { store } from './store'

Vue.use(Router)

export const router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'root',
      redirect: { name: 'login' }
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
      meta: {
        redirect: { auth: { name: 'account' } }
      }
    },
    {
      path: '/account',
      name: 'account',
      component: () => import('./views/Account.vue'),
      meta: {
        redirect: { noAuth: { name: 'login' } }
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  const { redirect } = to.meta

  if (redirect) {
    // redirect if authenticated
    if (redirect.auth && !!store.getters['auth/user']) next(redirect.auth)

    // redirect if not authenticated
    if (redirect.noAuth && !store.getters['auth/user']) next(redirect.noAuth)
  }

  next()
})
