import Vue from 'vue'
import VueApollo from 'vue-apollo'

import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { ApolloLink, split } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import introspectionQueryResultData from '@/generated/graphql.schema'

Vue.use(VueApollo)

const host = 'localhost:3000'

// HTTP connection to the API
const httpLink = createHttpLink({
  // You should use an absolute URL here
  uri: process.env.VUE_APP_GRAPHQL_HTTP || `http://${host}/graphql`,
  credentials: 'include'
})

// Create the subscription websocket link
const wsLink = new WebSocketLink({
  uri: process.env.VUE_APP_GRAPHQL_WS || `ws://${host}/graphql`,
  options: {
    reconnect: true
  }
})

// Split to correct link
const link = split(
  // split based on operation type
  ({ query }: {query: any}) => {
    const definition = getMainDefinition(query)
    return definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
  },
  wsLink,
  httpLink
)

const authLink = new ApolloLink((operation, forward) => {
  return forward(operation)
})

// Introspection Fragment Matcher
const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
})

// Cache implementation
const cache = new InMemoryCache({
  fragmentMatcher
})

// Create the apollo client
export const apolloClient = new ApolloClient({
  cache,
  link: authLink.concat(link)

})

export const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})
