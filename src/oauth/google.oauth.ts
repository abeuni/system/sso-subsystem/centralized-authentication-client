import ClientOauth2 from 'client-oauth2'
import { Oauth2Config } from '.'

const external = {
  clientId: process.env.VUE_APP_OAUTH_GOOGLE_CID,
  redirectUri: process.env.VUE_APP_OAUTH_GOOGLE_RED
}

typeof external.clientId === 'string' || console.error('Google Client ID failed to load')
typeof external.redirectUri === 'string' || console.error('Google Redirect URI failed to load')

export const oauthGoogle: Oauth2Config = {
  id: 'google',
  client: new ClientOauth2({
    ...external,
    authorizationUri: 'https://accounts.google.com/o/oauth2/v2/auth',
    scopes: ['email', 'profile']
  }),
  icon: 'mdi-google',
  label: 'Google'
}
