import ClientOAuth2 from 'client-oauth2'
import { oauthGoogle } from './google.oauth'

export interface Oauth2Config {
  id: string;
  client: ClientOAuth2;
  icon: string;
  label: string;
}

export const oauthClients: Oauth2Config[] = [
  oauthGoogle
]
